
import store from '@/src/components/redux/store/store';
import Header from '@/src/container/header';
import '@/styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Provider } from 'react-redux';

export default function App({ Component, pageProps }) {
  const queryClient = new QueryClient();
  return (
    <>
      <Provider store={store}>
        <QueryClientProvider client={queryClient}>

          <Header />
          <Component {...pageProps} />
        </QueryClientProvider>
      </Provider>
    </>
  )
}
