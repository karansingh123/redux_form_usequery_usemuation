import { Inter } from 'next/font/google'
const inter = Inter({ subsets: ['latin'] })
import PersonalDetails from '@/src/container/firstPage'
export default function Home() {
  return (
    <>
      <PersonalDetails />
    </>
  )
}
