export const UPDATE_STEP1 = 'UPDATE_STEP1';
export const UPDATE_STEP2 = 'UPDATE_STEP2';
export const UPDATE_STEP3 = 'UPDATE_STEP3';
export const TOGGLE_DARK_MODE = 'TOGGLE_DARK_MODE';

export const updatePersonalDetails = (formData) => {
    console.log(formData,'formdatastep1')
    return {
        type: UPDATE_STEP1,
        payload: formData
    };
};

export const updateEducationDetails = (formData) => {
    console.log(formData,'formdatastep2')
    return {
        type: UPDATE_STEP2,
        payload: formData
    }
}

export const updateAddressDetails = (formData) => {
    console.log(formData,'formdatastep3')
    return {
        type: UPDATE_STEP3,
        payload: formData
    }
}

export const toggleDarkMode = (mode) => ({
    type: TOGGLE_DARK_MODE,
    payload: mode,
});
