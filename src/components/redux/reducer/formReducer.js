
import { TOGGLE_DARK_MODE, UPDATE_STEP1, UPDATE_STEP2, UPDATE_STEP3 } from '../actiion/index';

const initialState = {
  step1: {
    PersonalDetails: {
      FirstName: '',
      LastName: '',
      Username: ''
    }
  },
  step2: {
    EducationDetails: {
      Qualification: '',
      Subject: '',
      Medium: '',
      Diploma: ''
    }
  },
  step3: {
    AddressDetails: {
      Address: '',
      City: '',
      State: '',
      PinCode: ''
    }
  },
  mode: 'light'
};

const formReducer = (state = initialState, action) => {
  
  console.log(action, "action")
  switch (action.type) {
    case UPDATE_STEP1:
      console.log(UPDATE_STEP1, 'updatetep1')
      return {
        ...state,
        step1: {
          ...state.step1,
          PersonalDetails: action.payload
        }

      };
    case UPDATE_STEP2:
      console.log(UPDATE_STEP2, 'updatestep2')
      return {
        ...state,
        step2: {
          ...state.step2,
          EducationDetails: action.payload
        }

      };

    case UPDATE_STEP3:
      console.log(UPDATE_STEP3, 'updatestep3')
      return {
        ...state,
        step3: {
          ...state.step3,
          AddressDetails: action.payload
        }

      };

    case TOGGLE_DARK_MODE:
      return {
        ...state,
        mode: state.mode === 'light' ? 'dark' : 'light',
        
      };

    default:
      return state;
  }


};

export default formReducer;
