import axios from "axios";

const GetcountryData = async () => {
    const response = await axios.get('https://restcountries.com/v3.1/all');
    return response.data;

};

export const PincodeData = async (pinCode) => {

    const response = await axios.get(`https://api.postalpincode.in/pincode/${pinCode}`);
    if (response.data && response.data.length > 0) {
        return response.data[0].PostOffice;
    }
   
}
export default GetcountryData;