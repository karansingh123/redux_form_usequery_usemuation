import React, { useEffect, useState } from 'react';
import { Button, Container } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import ProgressBar from '../progressBar';
import { useDispatch, useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { updatePersonalDetails } from '@/src/components/redux/actiion';
import { useMutation } from 'react-query';
import { usefirstPageHook } from './hooks';
import { PincodeData } from '@/src/components/api';

function PersonalDetails() {

  const {
    router,
    useRouter,
    dispatch,
    personalDetails,
    formData,
    setFormData,
    handleChange,
    activeStep,
    setActiveStep,
    handleSubmit,
    pinCode,
    setPinCode,
    data,
    setData,
    selectedValue,
    setSelectedValue,
    mutate: PincodeDataMutate,
    PincodeData,

  } = usefirstPageHook()

  return (
    <>
      <div className='mt-5 pt-5'>
        <ProgressBar activeStep={0} />
        <Container>
          <div className='d-flex justify-content-around mt-5' >
            <Form onSubmit={handleSubmit} className='w-75'>
              <Form.Group controlId="validationCustom01">
                <Form.Label>First name</Form.Label>
                <Form.Control
                  required
                  type="text"
                  placeholder="First name"
                  name="FirstName"
                  value={formData.FirstName}
                  onChange={handleChange}
                />
                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="validationCustom02">
                <Form.Label>Last name</Form.Label>
                <Form.Control
                  required
                  type="text"
                  placeholder="Last name"
                  name="LastName"
                  value={formData.LastName}
                  onChange={handleChange}
                />
                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="validationCustomUsername">
                <Form.Label>Username</Form.Label>
                <InputGroup hasValidation>
                  <InputGroup.Text id="inputGroupPrepend">@</InputGroup.Text>
                  <Form.Control
                    type="text"
                    placeholder="Username"
                    aria-describedby="inputGroupPrepend"
                    required
                    name="Username"
                    value={formData.Username}
                    onChange={handleChange}
                  />
                  <Form.Control.Feedback type="invalid">
                    Please choose a username.
                  </Form.Control.Feedback>
                </InputGroup>
              </Form.Group>
              <div className='mt-3'>
                <input
                  type="text"
                  value={pinCode}
                  onChange={(e) => setPinCode(e.target.value)}
                  placeholder='Enter PinCode here'
                />
                <Button variant="info" className='mx-2' onClick={() => PincodeDataMutate(pinCode)}>Get Data</Button>
                <h6>Select Post Office:</h6>

                <div>
                  <select
                    value={selectedValue}
                    onChange={(e) => setSelectedValue(e.target.value)}
                  >
                    {data?.map((postOffice) => (
                      <option key={postOffice.Name} value={postOffice.Name}>
                        {postOffice.Name}
                      </option>
                    ))}
                  </select>
                </div>

              </div>


              <Button className='mt-3' style={{ float: 'right' }} type="submit">Next</Button>
            </Form>
          </div>
        </Container>
      </div>
    </>
  );
}

export default PersonalDetails;