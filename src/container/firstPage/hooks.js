import React, { useEffect, useState } from 'react';
import { Button, Container } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import ProgressBar from '../progressBar';
import { useDispatch, useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { updatePersonalDetails } from '@/src/components/redux/actiion';
import { useMutation } from 'react-query';
import { PincodeData } from '@/src/components/api';
export const usefirstPageHook = () => {

    const router = useRouter();
    const dispatch = useDispatch();
    const personalDetails = useSelector(state => state.step1.PersonalDetails);
    console.log(personalDetails, 'step11');
    const [formData, setFormData] = useState({
        FirstName: '',
        LastName: '',
        Username: ''
    });
    const [activeStep, setActiveStep] = useState(0);

    useEffect(() => {
        setFormData(personalDetails);
    }, [personalDetails]);
    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData((prevData) => ({
            ...prevData,
            [name]: value,
        }));
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        setActiveStep((prevStep) => prevStep + 1);
        dispatch(updatePersonalDetails(formData));
        router.push('/secondPage');
    };
    const [pinCode, setPinCode] = useState('');
    const [data, setData] = useState([]);
    const [selectedValue, setSelectedValue] = useState('');
    const { mutate: PincodeDataMutate } = useMutation(
        (pinCode) => PincodeData(pinCode),
        {
            onSuccess: (data) => {
                try {
                    setData(data);
                } catch (error) {
                    console.error('Error fetching pin code data:', error);
                    return [];
                }
            }
        }
    );


    return {
        router,
        useRouter,
        dispatch,
        personalDetails,
        formData,
        setFormData,
        personalDetails,
        handleChange,
        handleSubmit,
        activeStep,
        setActiveStep,

        pinCode,
        setPinCode,
        data,
        setData,
        selectedValue,
        setSelectedValue,
        mutate: PincodeDataMutate,
        PincodeData,
    }
}