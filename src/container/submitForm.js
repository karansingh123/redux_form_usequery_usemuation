import Link from 'next/link';
import React, { useReducer } from 'react';
import {  Container } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';

const FinalSubmit = () => {
  const formData = ""
  const dispatch = useDispatch();
  const personalDetails = useSelector(state => state.step1.PersonalDetails);
  console.log(personalDetails, 'submitvalue')
  const educationDetails = useSelector(state => state.step2.EducationDetails);
  const addressDetails = useSelector(state => state.step3.AddressDetails);
  const router = useReducer()


  return (
    <div >
      <Container>
        <div >
          <h1 className='text-center'>Submitted Data</h1>
          <Link className='m-4 bg-warning py-2 px-3  text-light' style={{ float: 'right', textDecoration: 'none', borderRadius: '10px' }} href={'/'}>Home</Link>

          <h2 className='mt-5'>Personal Details:</h2>
          {Object.keys(personalDetails)?.map((step1) => {
            return (
              <div>
                {step1}: {personalDetails[`${step1}`]}
              </div>
            )
          })}

          <h2>Education Details:</h2>
          {Object.keys(educationDetails)?.map((step2) => {
            return (
              <div>
                {step2}: {educationDetails[`${step2}`]}
              </div>
            )
          })}

          <h2>Address Details:</h2>
          {Object.keys(addressDetails)?.map((step3) => {
            return (
              <div>
                {step3}: {addressDetails[`${step3}`]}
              </div>
            )
          })}
        </div>

      </Container>
    </div>
  );
};

export default FinalSubmit;