import { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useDispatch, useSelector } from 'react-redux';
import ProgressBar from '../progressBar';
import { useRouter } from 'next/router';
import { updateAddressDetails } from '@/src/components/redux/actiion';


function AddressDetails() {
  const dispatch = useDispatch();
  const router = useRouter();
  const addressDetails = useSelector(state => state.step3.AddressDetails);
  console.log(addressDetails, 'step33')

  useEffect(() => {
    setFormData(addressDetails);
  }, [addressDetails]);

  const [formData, setFormData] = useState({
    Address: '',
    City: '',
    State: '',
    PinCode: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const [activeStep, setActiveStep] = useState(0);
  const handleSubmit = (event) => {
    event.preventDefault();
    setActiveStep((prevStep) => prevStep - 2);
    dispatch(updateAddressDetails(formData));

  };

  // console.log(formData, 'step3');
  const FinalSubmit = () => {
    router.push('/submitForm');
  }
  return (
    <>
      <div className='mt-5 pt-5'>
        <ProgressBar activeStep={2} />
        <Container>
          <div className='d-flex justify-content-around'>
            <Form onSubmit={handleSubmit} className='w-75'>
              <Form.Group className="mb-3" controlId="formGridAddress2">
                <Form.Label>Address</Form.Label>
                <Form.Control placeholder="Apartment, studio, or floor"
                  required
                  name="Address"
                  value={formData?.Address}
                  onChange={handleChange} />
              </Form.Group>
              <Form.Group controlId="formGridCity">
                <Form.Label>City</Form.Label>
                <Form.Control placeholder='City'
                  required
                  name="City"
                  value={formData?.City}
                  onChange={handleChange} />

              </Form.Group>
              <Form.Group controlId="formGridState">
                <Form.Label>State</Form.Label>
                <Form.Control placeholder='State'
                  required
                  name="State"
                  value={formData?.State}
                  onChange={handleChange} />

              </Form.Group>
              <Form.Group controlId="formGridZip">
                <Form.Label>PinCode</Form.Label>
                <Form.Control placeholder='Pincode'
                  required
                  name="PinCode"
                  value={formData?.PinCode}
                  onChange={handleChange} />
              </Form.Group>
            
              

              <Button className='mt-3' style={{ float: 'right' }} onClick={FinalSubmit} variant="primary" type="submit">
                Submit
              </Button>
              <Button className='mt-3' onClick={() => router.push('/secondPage')}>Previous</Button>

            </Form>

          </div>
        </Container>
      </div>
    </>
  );
}

export default AddressDetails;