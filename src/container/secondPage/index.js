import { useEffect, useState } from "react";
import { Container, FormControl } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import { useDispatch, useSelector } from "react-redux";
import ProgressBar from "../progressBar";
import { useRouter } from "next/router";
import { updateEducationDetails } from "@/src/components/redux/actiion";

const EducationDetails = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const educationDetails = useSelector(state => state.step2.EducationDetails);
  console.log(educationDetails, 'step22');

  useEffect(() => {
    setFormData(educationDetails);
  }, [educationDetails]);
  const [formData, setFormData] = useState({
    Qualification: '',
    Subject: '',
    Medium: '',
    Diploma: ''
  });

  console.log(formData, 'qqqq')
  const handleChange = (e) => {

    const { name, value } = e.target;
    console.log(name, value, e.target, 'aaaaaaaa')
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const [activeStep, setActiveStep] = useState(0);
  const handleSubmit = (event) => {

    event.preventDefault();

    setActiveStep((prevStep) => prevStep + 1);
    dispatch(updateEducationDetails(formData));
    router.push('/thirdPage');
  };


  // console.log(formData, 'step2');

  return (
    <>
      <div className='mt-5 pt-5'>
        <ProgressBar activeStep={1} />
        <Container>
          <div className='d-flex justify-content-around' >
            <Form noValidate onSubmit={handleSubmit} className="w-75">
              <div>
                <Form.Group as={Col} controlId="validationCustom01">
                  <Form.Label>Qualification</Form.Label>

                  {['radio'].map((type) => (
                    <div key={`inline-${type}`} className="mb-3">

                      {["10th", '12th', 'Graduation', 'PostGraduation'].map((item, index) => {
                        return (
                          <Form.Check
                            inline
                            checked={formData?.Qualification == item ? true : false}
                            label={item}
                            name="Qualification"
                            type='radio'
                            id={`inline-${type}-1`}
                            value={item}
                            onChange={handleChange}
                          />
                        )
                      })}
                    </div>
                  ))}

                  <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                </Form.Group>
              </div>

              <Form.Group as={Col} controlId="validationCustom02">
                <Form.Label>Qualification Subject</Form.Label>
                <Form.Select aria-label="Default select example" name="Subject" value={formData?.Subject}
                  onChange={handleChange}>
                  <option>Qualification Subject</option>
                  <option value="Math">Math</option>
                  <option value="Computer">Computer</option>
                  <option value="English">English</option>
                  <option value="Science">Science</option>
                </Form.Select>
                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
              </Form.Group>
              <Form.Group as={Col} controlId="validationCustom03">
                <Form.Label>Medium</Form.Label>
                <Form.Control
                  required
                  type="text"
                  placeholder="Medium"
                  name="Medium"
                  value={formData?.Medium}
                  onChange={handleChange} />
                <Form.Control.Feedback >
                  Looks good!
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group as={Col} controlId="validationCustom04">
                <Form.Label>Diploma</Form.Label>
                <Form.Control
                  required
                  type="text"
                  placeholder="Diploma"
                  name="Diploma"
                  value={formData?.Diploma}
                  onChange={handleChange} />
                <Form.Control.Feedback  >
                  Looks good!
                </Form.Control.Feedback>
              </Form.Group>
              <Button className='mt-3 ' variant="outline-info" onClick={() => router.push('/firstPage')} >Previous</Button>
              <Button className='mt-3' style={{ float: 'right' }} type="submit">Next</Button>
            </Form>


          </div>
        </Container>
      </div>
    </>
  )
}

export default EducationDetails