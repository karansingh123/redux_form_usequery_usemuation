import React from 'react';
import styles from '../../styles/Home.module.css';
import { useSelector } from 'react-redux';

const ProgressBar = ({ activeStep }) => {
  const steps = ['Personal Details', 'Education Details', 'Address Details'];
  const progressBarWidth = (activeStep / (steps.length - 1)) * 65;


  return (
    <div className={`d-flex justify-content-around ${styles.progressBar}`}>
      <div className={styles.progressbar} style={{ width: `${progressBarWidth}%` }}></div>
      <div className={styles.progress }></div>
      {steps.map((step, index) => (
        <div key={index}>
          <p
            className={`${styles.step} ${index <= activeStep ? styles.active : ''} text-black `} style={{
              backgroundColor: index === activeStep ? 'orangered' : '',
            }}
          >
            {index + 1}
          </p>
          <p  className={`${index <= activeStep ? styles.steps : ''}`}>{step}</p>
        </div>
      ))}
    </div>
  );
};

export default ProgressBar;


