import React from 'react';
import { Container, Navbar, Dropdown } from 'react-bootstrap';
import { useQuery } from 'react-query';
import GetcountryData from '../components/api'


function Header() {
	const { data, status } = useQuery('countries', GetcountryData);
	console.log(data, 'data')

	if (status === 'loading') {
		return <p>Loading...</p>;
	}
	if (status === 'error') {
		return <p>Error fetching data</p>;
	}
	return (
		<>
			<Container>
				<div className='mb-5 '>
					<Navbar expand="lg" fixed='top' className=" bg-body-tertiary" >
						<Container >
							<Navbar.Brand href="/">Redux-Form</Navbar.Brand>
							<div>
								<Dropdown>
									<Dropdown.Toggle variant="success" id="country-dropdown">
										Select a Country
									</Dropdown.Toggle>
									<Dropdown.Menu>
										{
											data.map((country) => (
												<Dropdown.Item key={country.cca2.caa3}>
													{country.name.common} </Dropdown.Item>
											))
										}
									</Dropdown.Menu>
								</Dropdown>

							</div>
						</Container>
					</Navbar>
				</div>
			</Container>
		</>
	);
}

export default Header;
